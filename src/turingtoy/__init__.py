from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

# Obtient version du package
__version__ = poetry_version.extract(source_file=__file__)

def clean_blank(input_, blank):
    # Supprime blancs au debut
    i = 0
    while(input_[i] == blank):
        input_.pop(i)
        i += 1
    # Supprime blancs a la fin
    i = 0
    len_input = len(input_)
    while(input_[len_input-i-1] == blank):
        input_.pop(len_input-i-1)
        i += 1
    return input_

def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    # Affiche machine, input_ et steps
    print(f"here is the machine: {machine}\nhere is the input_: {input_}\nhere is the steps: {steps}")

    blank = machine['blank']
    start = machine['start state']
    end = machine['final states']
    table = machine['table']

    # Affiche debug info
    print(f"table blank == {blank}")
    print(f"table start at {start} == {table[start]}")
    print(f"table end at {end} == {table[end[0]]}")

    curr_state = start
    state_dict = table[curr_state]
    pos = 0

    input_ = list(input_)
    old_inp = input_
    i = 0

    history = []
    old_pos = 0
    print(curr_state)
    old_state = curr_state
    while (curr_state not in end):
        for step in state_dict:  # Pour chaque etape
            if(pos < 0):
                pos = 0
                input_.insert(0, blank)
            try:
                input_[pos]
            except:
                input_.append(blank)
            if(step == input_[pos]):
                # Gere instruction 'write' et deplacement
                if (type(state_dict[step]) is dict and list(state_dict[step])[0] == "write"):
                    instru = state_dict[step]
                    old_inp = input_.copy()
                    input_[pos] = state_dict[step]['write']
                    direction = list(state_dict[step])[1]
                    old_pos = pos
                    pos += -1 if direction == "L" else 1
                    old_state = curr_state
                    curr_state = list(state_dict[step].values())[1]
                    state_dict = table[curr_state]
                    break
                # Gere instruction 'R' (droite)
                elif (type(state_dict[step]) is dict and list(state_dict[step])[0] == "R"):
                    instru = state_dict[step]
                    old_inp = input_.copy()
                    old_pos = pos
                    pos += 1
                    old_state = curr_state
                    curr_state = list(state_dict[step].values())[0]
                    state_dict = table[curr_state]
                    break
                # Gere instruction 'L' (gauche)
                elif (type(state_dict[step]) is dict and list(state_dict[step])[0] == "L"):
                    instru = state_dict[step]
                    old_inp = input_.copy()
                    old_pos = pos
                    pos += -1
                    old_state = curr_state
                    curr_state = list(state_dict[step].values())[0]
                    state_dict = table[curr_state]
                    break
                # Gere deplacement a droite
                elif (state_dict[step] == "R"):
                    instru = state_dict[step]
                    old_inp = input_.copy()
                    old_pos = pos
                    pos += 1
                    break
                # Gere deplacement a gauche
                elif (state_dict[step] == "L"):  # pragma: no branch
                    instru = state_dict[step]
                    old_inp = input_.copy()
                    old_pos = pos
                    pos += -1
                    break

        i += 1
        # Enregistre etat dans historique
        hist_dic = {
            "state": old_state,
            "reading": old_inp[old_pos],
            "position": old_pos,
            "memory": ''.join(old_inp),
            "transition": instru
        }
        old_state = curr_state
        history.append(hist_dic)
        print(hist_dic)
    # Nettoie input en supprimant blancs
    input_ = clean_blank(input_, blank)
    return (''.join(input_), history, True)
